<?php
	// stats:
	// https://na.api.pvp.net/api/lol/na/v1.3/stats/by-summoner/34563173/ranked?season=SEASON4&api_key=5fde7076-031e-4697-af19-89d851f4ef1d

	$HTTPS = 'https://';
	$API = '.api.pvp.net/api/lol/';
	$VERSION = '/v1.3/stats/by-summoner/';
	$API_KEY = '/ranked?season=SEASON4&api_key=5fde7076-031e-4697-af19-89d851f4ef1d';

	if(isset($_GET['summonerId']) && isset($_GET['server'])) {
		$summonerId = $_GET['summonerId'];
		$server = $_GET['server'];

		// build url
		$url = $HTTPS . $server . $API . $server . $VERSION . $summonerId . $API_KEY;
		$handle = curl_init();
		curl_setopt_array($handle, array( CURLOPT_URL => $url));
		curl_exec($handle);
		curl_close($handle);
	}
?>