<?php
	// player ingame info
	// https://community-league-of-legends.p.mashape.com/api/v1.0/NA/summoner/retrieveInProgressSpectatorGameInfo/wingsofdeathx
	// "X-Mashape-Key" => "IzTy2Oe9GKmshWRxvoB3cKygwcdgp1Dw3B4jsnQc3MSznMj1aH"

	$HTTPS = 'https://community-league-of-legends.p.mashape.com/api/v1.0/';
	$API = '/summoner/retrieveInProgressSpectatorGameInfo/';
	$API_KEY = 'X-Mashape-Key: IzTy2Oe9GKmshWRxvoB3cKygwcdgp1Dw3B4jsnQc3MSznMj1aH';

	if(isset($_GET['input']) && isset($_GET['server'])) {
		$input = $_GET['input'];
		$server = $_GET['server'];

		// build url
		$url = $HTTPS . $server . $API . $input;
		$handle = curl_init();
		curl_setopt_array($handle, array( CURLOPT_URL => $url));
		curl_setopt($handle, CURLOPT_HTTPHEADER, array( $API_KEY));
		curl_exec($handle);
		curl_close($handle);
	}
?>