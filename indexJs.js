// global variables set dynamically
var SERVER_NAME = "";

// listen for enter key hit
function runOnEnterHit(e) {
	if(e.keyCode === 13) {
		processInput();
	} 
}

// listen for search button hit
$("#search").click(function() { processInput(); });

// process dropdown selection
$(".dropdown-menu li").click(function(){
	var serverAbb = processServerName($(this).text()) + " ";
	SERVER_NAME = serverAbb.toLowerCase(); // set global SERVER_NAME whenever the selection is made
	$(".dropdown-toggle b").remove().appendTo($('.dropdown-toggle').text(serverAbb));
});

// get called from either hit search button or hit enter key
function processInput() {
	var input = $("#input").val().toLowerCase().toString().replace(/\s/g, "");
	var server = SERVER_NAME.toLowerCase().toString().trim();

	// set default server to NA
	if(server === ""){ server = "na"; }
	if(input !== "") {
		$.get('summonerProcess.php', { input: input, server: server, }, function(data) {

			var obj = JSON.parse(data);
			var userObj = obj[input];
			var summonerId = userObj.id;

			// display sample out put user id, name, and level
			$("#summoner").html('Name: ' + userObj.name + "</br>");
			$("#summoner").append('ID: ' + summonerId + "</br>");
			$("#summoner").append('Level: ' + userObj.summonerLevel + "</br>");

			$("#inGameStatus").html('Currently in game? Checking...');

			// show stats for each champion 
			if(summonerId !== "") {

				// get champion info
				var championTemp = {};
				var championPollArr = [];

				$.get('getChampionId.php', { server: server, }, function(data) {
					obj = JSON.parse(data);

					championTemp = obj.data;
					
					// get number of champions in data object
					var length = 0;
					for(var obj in championTemp) { 
						length++;
						var curchampionPollArr = championTemp[obj];

						championPollArr.push({
							name: obj,
							id: curchampionPollArr.id,
							title: curchampionPollArr.title
						});
					}

					// get stats based on summoner id and server
					$.get('statsProcess.php', { summonerId: summonerId, server: server, }, function(data) {
						drawTable(data, championPollArr);
					});


					// get live games stats based on player name and server selection 
					$.get('getPlayerIngameInfo.php', { input: input, server: server, }, function(data) {
						obj = JSON.parse(data);
						console.log(obj);
						setInGameStatus(obj); // check if the player is currently in a game
					});
				});
			}
		});
	} 
	else { 
		console.log("input is empty"); 
	}
}

function setInGameStatus(obj){
	if(obj.success !== undefined && obj.success === "false") {
		console.log("not in game");

		// var text = $("#summoner").text();
		// text = text.replace("Checking...", "No");
		// $('#summoner').text(text);

		$("#inGameStatus").html('Currently in game? No');

	}
	else if(obj.gameName !== undefined && obj.game.gameState === "IN_PROGRESS") {
		console.log("yes in game");

		// var text = $("#summoner").text();
		// text = text.replace("Checking...", "Yes");
		// $('#summoner').text(text);

		$("#inGameStatus").html('Currently in game? Yes');
	}
}

// draw table with parameter data and championPollArr
function drawTable(data, championPollArr) {
	obj = JSON.parse(data);
						
	var championSize = obj.champions.length;

	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Champion ID');
	data.addColumn('string', 'Champion Name');
	data.addColumn('number', 'Win');
	data.addColumn('number', 'Lose');
	data.addColumn('string', 'Win %');

	for(var i = 0; i < championSize; i++) {
		var curchampionPollArr = obj.champions[i]
		var curChampionId = (curchampionPollArr.id === 0) ? "" : curchampionPollArr.id.toString();
		var championName = getChampionName(parseInt(curChampionId), championPollArr);

		var championWin = curchampionPollArr.stats.totalSessionsWon;
		var championLose = curchampionPollArr.stats.totalSessionsLost;
		var winRaioTemp = (championWin / (championWin + championLose));
		var winRaio = (winRaioTemp === 0) ? "0" : (winRaioTemp*100).toFixed(2);
		
		data.addRows([[
			{v: curChampionId,	f: curChampionId.toString()}, 
			{v: championName, 	f: championName}, 
			{v: championWin,  	f: championWin.toString()}, 
			{v: championLose, 	f: championLose.toString()},
			{v: winRaio, 	  	f: winRaio.toString() + "%"}
		]]);
	}

	// sort table based on index, win is index 2, index 0 champion id is hidden, but it's there
	data.sort({column: 2, desc: true});

	var view = new google.visualization.DataView(data);
	view.setColumns([1,2,3,4]); //here set columns you want to display (hide chanmpion id with index 0)

	var table = new google.visualization.Table(document.getElementById('stats'));
	
	var options = {
		showRowNumber: false,
	};

	table.draw(view, options);
	
	addAutoResize(view, options, table)
}

// helper function take current champion id and champion poll and return name
function getChampionName(curChampionId, championPollArr) {
	for(i = 0; i < championPollArr.length; i++){
		if(championPollArr[i].id === curChampionId) {
			return championPollArr[i].name;
		}
	}
}

// support charts responsive when resizing window
function addAutoResize(data, options, chart) {
	function resizeHandler () {
		chart.draw(data, options);
	}
	if (window.addEventListener) {
		window.addEventListener('resize', resizeHandler, false);
	}
	else if (window.attachEvent) {
		window.attachEvent('onresize', resizeHandler);
	}
}

// update server name based on dropdown selection 
function processServerName(serverName) {
	var lowerCaseName = serverName.toLowerCase();
	var serverAbb = "";
	if     (lowerCaseName === "north america ")			{ serverAbb = "NA"; } 
	else if(lowerCaseName === "europe west ")			{ serverAbb = "EUW"; }
	else if(lowerCaseName === "europe nordic & east ")	{ serverAbb = "EUNE"; }
	else if(lowerCaseName === "brazil ")				{ serverAbb = "BR"; } 
	else if(lowerCaseName === "turkey ")				{ serverAbb = "TR"; } 
	else if(lowerCaseName === "russia ")				{ serverAbb = "RU"; }
	else if(lowerCaseName === "latin america north ")	{ serverAbb = "LAN"; }
	else if(lowerCaseName === "latin america south ")	{ serverAbb = "LAS"; }
	else if(lowerCaseName === "oceania ")				{ serverAbb = "OCE"; }
	return serverAbb;
}
