<!DOCTYPE html>
<html>

<head>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<!-- load google chart api -->
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
      google.load("visualization", "1", {packages:["table"]});
    </script>
    
	<!-- include native local css from css folder -->
	<link rel="stylesheet" href="css/style.css">

    <title>Loject</title>
</head>

<div id="header"></div>
<body>

	<div class="container">

		<div class="col-lg-6 col-centered">

			<div class="input-group" role="form" action="summonerProcess.php" method="get">

				<span class="input-group-btn">
					<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown">NA <b class="caret"></b></button>
					
					<ul class="dropdown-menu">
						<li><a>North America </a></li>
						<li><a>Europe West </a></li>
						<li><a>Europe Nordic &amp; East </a></li>
						<li><a>Brazil </a></li>
						<li><a>Turkey </a></li>
						<li><a>Russia </a></li>
						<li><a>Latin America North </a></li>
						<li><a>Latin America South </a></li>
						<li><a>Oceania </a></li>
					</ul>
				</span>

				<input class="form-control" 
						type="search" 
						id="input" 
						autofocus="true" 
						placeholder="Summoner name"
						onkeypress="return runOnEnterHit(event)" /> 
				</input>

			
				<span class="input-group-btn">
					<button class="btn btn-primary" type="button" id="search">Search</button>
				</span>
			</div>
		
		</div></br></br>

		<div id="summoner"></div>
		<div id="inGameStatus"></div>
		<div id="stats"></div>

	</div>

	<!-- include js file  -->
	<script src="indexJs.js"> </script>

</body>
<div id="footer"></div>

</html>
