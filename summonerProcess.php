<?php
	// summoner:
	// https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/mtyonboard?api_key=5fde7076-031e-4697-af19-89d851f4ef1d

	$HTTPS = 'https://';
	$API = '.api.pvp.net/api/lol/';
	$VERSION = '/v1.4/summoner/by-name/';
	$API_KEY = '?api_key=5fde7076-031e-4697-af19-89d851f4ef1d';

	if(isset($_GET['input']) && isset($_GET['server'])) {
		$input = $_GET['input'];
		$server = $_GET['server'];

		// build url
		$url = $HTTPS . $server . $API . $server . $VERSION . $input . $API_KEY;
		$handle = curl_init();
		curl_setopt_array($handle, array( CURLOPT_URL => $url));
		curl_exec($handle);
		curl_close($handle);
	}
?>